# Qt_5.13_RPi_eglfs

Qt 5.13 EGLFS

cross-compiled for  armv7-a (raspberry pi 2 or upper) 

this version contains qtwebengine
and dosn't include qtwayland , qtx11extras and qtwinextras

sysroot: raspbian jessie
compiler: gcc-linaro-7.4.1-2019.02-x86_64_arm-linux

libs prefix dir is setted at: /usr/local/qt5pi

compiled through this guide:
https://wiki.qt.io/RaspberryPi2EGLFS
