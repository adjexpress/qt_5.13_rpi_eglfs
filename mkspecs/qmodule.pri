host_build {
    QT_CPU_FEATURES.x86_64 = mmx sse sse2
} else {
    QT_CPU_FEATURES.arm = neon
}
QT.global_private.enabled_features = alloca_h alloca dbus dbus-linked gui libudev network posix_fallocate reduce_exports sql system-zlib testlib widgets xml
QT.global_private.disabled_features = sse2 alloca_malloc_h android-style-assets avx2 private_tests gc_binaries reduce_relocations release_tools stack-protector-strong zstd
PKG_CONFIG_EXECUTABLE = /bin/pkg-config
QMAKE_LIBS_DBUS = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libdbus-1.so
QMAKE_INCDIR_DBUS = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/include/dbus-1.0 /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/dbus-1.0/include
QMAKE_LIBS_LIBUDEV = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/lib/arm-linux-gnueabihf/libudev.so
QT_COORD_TYPE = double
QMAKE_LIBS_ZLIB = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libz.so
CONFIG += cross_compile compile_examples enable_new_dtags largefile neon precompile_header
QT_BUILD_PARTS += libs
QT_HOST_CFLAGS_DBUS += -I/usr/include/dbus-1.0 -I/usr/lib/dbus-1.0/include
