QT.core_private.VERSION = 5.13.0
QT.core_private.name = QtCore
QT.core_private.module =
QT.core_private.libs = $$QT_MODULE_LIB_BASE
QT.core_private.includes = $$QT_MODULE_INCLUDE_BASE/QtCore/5.13.0 $$QT_MODULE_INCLUDE_BASE/QtCore/5.13.0/QtCore
QT.core_private.frameworks =
QT.core_private.depends = core
QT.core_private.uses =
QT.core_private.module_config = v2 internal_module
QT.core_private.enabled_features = clock-gettime datetimeparser dlopen doubleconversion futimens getauxval glib glibc posix-libiconv icu inotify linkat mimetype-database poll_ppoll sha3-fast system-doubleconversion
QT.core_private.disabled_features = etw futimes getentropy gnu-libiconv iconv journald lttng poll_poll poll_pollts poll_select renameat2 slog2 statx syslog system-pcre2
QMAKE_LIBS_LIBRT = 
QMAKE_LIBS_LIBDL = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libdl.so
QMAKE_LIBS_GLIB = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libgthread-2.0.so /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libglib-2.0.so
QMAKE_INCDIR_GLIB = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/include/glib-2.0 /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/glib-2.0/include
QMAKE_LIBS_ICONV = 
QMAKE_DEPENDS_ICU_CC = LIBDL
QMAKE_DEPENDS_ICU_LD = LIBDL
QMAKE_LIBS_ICU = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libicui18n.so /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libicuuc.so /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libicudata.so
QMAKE_LIBS_LIBATOMIC = 
QMAKE_LIBS_DOUBLECONVERSION = /home/abdorahmanamani/workspace/Linux/rpi/sysroot/usr/lib/arm-linux-gnueabihf/libdouble-conversion.so
